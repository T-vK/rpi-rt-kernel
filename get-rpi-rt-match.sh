#!/bin/bash

# create array of x.x Kernel versions for which rt is available
readarray -t rtVersions <<<"$(curl -sS https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/ | grep -ioE "\"([0-9]+.[0-9]+/)")"
for ((i=0; i<${#rtVersions[@]}; i++)); do
    rtVersions[$i]=${rtVersions[$i]:1:-1}
done
# sort the array of versions (latest version = first entry)
readarray -t rtVersionsSorted < <(for a in "${rtVersions[@]}"; do echo "$a"; done | sort -r -V)

# iterate over the sorted x.x versions
for rtVersion in "${rtVersionsSorted[@]}"; do  
    echo "$rtVersion";
    # create an array of patch-x.x.x-rtx.patch.gz versions that are available for the current x.x version
    readarray -t rtFullVersions <<<"$(curl -sS https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/$rtVersion/older/ | grep -ioE "\"patch-[0-9]+.[0-9]+.[0-9]+\\-\\w+\\.patch\\.gz")"
    for ((i=0; i<${#rtFullVersions[@]}; i++)); do
        rtFullVersions[$i]=${rtFullVersions[$i]:1}
        #echo ${rtFullVersions[$i]}
    done
    # sort the array (latest version = first entry)
    readarray -t rtFullVersionsSorted < <(for a in "${rtFullVersions[@]}"; do echo "$a"; done | sort -r -V) 
    # iterate over the file file names
    for rtFullVersion in "${rtFullVersionsSorted[@]}"; do
        # extract the x.x.x version fromt he file name
        fullVersionOnly=$(echo "$rtFullVersion" | grep -ioE "[0-9]+\\.[0-9]+\\.[0-9]+")
        echo $fullVersionOnly
        # check on github if there has been a commit for this exact x.x.x version in the corresponsing rpi-x.x.y branch
        readarray -t rpiVersions <<<"$(curl -sS https://github.com/raspberrypi/linux/commits/rpi-$rtVersion.y/Makefile | grep -ioE "commit/[^>]+>Linux\\s+$fullVersionOnly")"
        # extract the commit hash from that result
        commitHash=$(echo "$rpiVersions[0]" | grep -ioE "/[^#]+#")
        commitHash=${commitHash:1:-1}
        echo $commitHash
        #for ((i=0; i<${#rpiVersions[@]}; i++)); do
        #    #rpiVersions[$i]=${rpiVersions[$i]:1}
        #    echo ${rpiVersions[$i]}
        #done

        if [[ $commitHash ]]; then
             export COMMIT_HASH=$commitHash
             export RT_PATCH_URL="https://www.kernel.org/pub/linux/kernel/projects/rt/$rtVersion/older/$rtFullVersion"
             export KERNEL_VERSION=$fullVersionOnly
             export KERNEL_XX_VERSION=$rtVersion
             break 2 # success
        fi  
        
    done
done
if [ -z "$COMMIT_HASH" ]; then
    exit 1
elif [ -z "$RT_PATCH_URL" ]; then
    exit 1
fi
